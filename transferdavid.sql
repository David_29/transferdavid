-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-05-2020 a las 12:53:27
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `transferdavid`
--
CREATE DATABASE IF NOT EXISTS `transferdavid` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `transferdavid`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `leagues`
--

CREATE TABLE `leagues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `leagues`
--

INSERT INTO `leagues` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Primera División Española 2018/2019', NULL, NULL),
(2, 'Primera División Española 2017/2018', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matches`
--

CREATE TABLE `matches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_team_local` bigint(20) UNSIGNED NOT NULL,
  `id_team_visitor` bigint(20) UNSIGNED NOT NULL,
  `id_league` bigint(20) UNSIGNED NOT NULL,
  `journey` int(11) NOT NULL,
  `score_local` int(11) NOT NULL,
  `score_visitor` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `matches`
--

INSERT INTO `matches` (`id`, `id_team_local`, `id_team_visitor`, `id_league`, `journey`, `score_local`, `score_visitor`, `created_at`, `updated_at`) VALUES
(3, 1, 2, 1, 1, 2, 1, '2020-05-05 08:14:23', '2020-05-05 08:14:23'),
(4, 3, 1, 1, 2, 3, 0, '2020-05-05 08:14:50', '2020-05-05 08:14:50'),
(5, 2, 3, 1, 2, 3, 4, '2020-05-05 08:16:57', '2020-05-05 08:16:57'),
(6, 1, 2, 2, 134, 1, 4, '2020-05-05 15:04:32', '2020-05-05 15:04:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2020_03_31_104849_create_team_table', 1),
(6, '2020_03_31_104913_create_player_table', 1),
(7, '2020_03_31_104938_create_league_table', 1),
(114, '2014_10_12_000000_create_users_table', 2),
(115, '2014_10_12_100000_create_password_resets_table', 2),
(116, '2019_08_19_000000_create_failed_jobs_table', 2),
(117, '2020_03_31_103405_create_notice_table', 2),
(118, '2020_03_31_112332_create_league_table', 2),
(119, '2020_03_31_113022_create_teams_table', 2),
(120, '2020_03_31_113056_create_players_table', 2),
(121, '2020_04_16_102910_create_participate_table', 2),
(122, '2020_04_16_104141_create_match_table', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notices`
--

CREATE TABLE `notices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtitle` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(3000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notices`
--

INSERT INTO `notices` (`id`, `id_user`, `title`, `subtitle`, `description`, `image`, `created_at`, `updated_at`) VALUES
(5, 2, 'prueba', 'pruebita', 'hola \r\n\r\nhola hola', 'storage/images/EyaIvTZTZaUaaf7W7Z23WgmLhhnAyaCnFHOEYVjZ.png', '2020-05-09 10:26:53', '2020-05-09 10:26:53'),
(6, 2, 'agag', 'aegg', 'aegag', 'storage/images/ibK1aE922m2g0HxqCEEPeKR46hd2nuXXbj7O8BQu.png', '2020-05-09 10:40:19', '2020-05-09 10:40:19'),
(7, 2, 'faefv', 'aegag', 'aegva', 'storage/images/VgrSm40wMPoI7eKCTR1Jm1Ae8IDCRZb4ck4YNKe8.jpeg', '2020-05-09 10:47:49', '2020-05-09 10:47:49'),
(8, 1, 'wq4tgwg', 'grwg', 'wg4w4r', 'storage/images/pUHhS41uQlKdGYvpAu6xssEVGFxOudNABqxkonEP.jpeg', '2020-05-09 10:59:23', '2020-05-09 10:59:23'),
(9, 1, 'Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'storage/images/yV49N1xwhYzjFP1IuERp8Dif7KkHdU9XTL6Hobai.jpeg', '2020-05-10 07:50:25', '2020-05-10 07:50:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participates`
--

CREATE TABLE `participates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_league` bigint(20) UNSIGNED NOT NULL,
  `id_team` bigint(20) UNSIGNED NOT NULL,
  `pj` int(11) NOT NULL,
  `v` int(11) NOT NULL,
  `e` int(11) NOT NULL,
  `d` int(11) NOT NULL,
  `gf` int(11) NOT NULL,
  `gc` int(11) NOT NULL,
  `dg` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `participates`
--

INSERT INTO `participates` (`id`, `id_league`, `id_team`, `pj`, `v`, `e`, `d`, `gf`, `gc`, `dg`, `points`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 12, 6, 3, 3, 32, 24, 8, 37, NULL, '2020-05-05 08:14:50'),
(2, 2, 1, 16, 10, 5, 4, 21, 16, 5, 22, NULL, '2020-05-05 15:04:32'),
(3, 1, 2, 21, 15, 5, 5, 6, 30, -24, 55, NULL, '2020-05-05 08:16:57'),
(4, 2, 2, 40, 12, 5, 4, 2, 45, 29, 33, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('d290397@hotmail.es', '$2y$10$vvgiXxlBjYfUtgCPeSUpWu4Sh6UEGosLrOwXzFaonwpp8Ql46uN4e', '2020-05-11 07:41:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `players`
--

CREATE TABLE `players` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_team` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  `player_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `players`
--

INSERT INTO `players` (`id`, `id_team`, `name`, `last_name`, `position`, `value`, `player_image`, `created_at`, `updated_at`) VALUES
(1, 1, 'Karim', 'Benzema', 'Delantero', 32000000, 'storage/images/LVt0zih0DcinAI4FvNlxnNnUoZRUuZV4olaHmJAE.png', '2020-05-05 08:17:49', '2020-05-06 08:53:33'),
(3, 1, 'Thibaut', 'Courtois', 'Portero', 48000000, 'storage/images/BrOQBXiwRBsRFmYQwBV0njhN85JQbCSStZqRoKae.png', '2020-05-05 08:18:38', '2020-05-05 08:18:38'),
(4, 2, 'Marc-André', 'ter Stegen', 'Portero', 72000000, 'storage/images/a2MQrcKLyoQgNH5cMzwjMYIVAlRAkPzlA4crvcNs.png', '2020-05-05 08:21:08', '2020-05-05 08:21:08'),
(5, 3, 'Joaquín', 'Sánchez', 'Centrocampista', 1600000, 'storage/images/eMPa1ucEi00INZesr1p3aSZ6IJwLxKBCNY6gjWZt.png', '2020-05-05 08:24:41', '2020-05-06 08:53:53'),
(6, 3, 'Joel', 'Robles', 'Portero', 6000000, 'storage/images/TyaCcVObJjjCQ11KSFsoqKp6lkjGQM1f2eCJoyqH.png', '2020-05-05 08:25:55', '2020-05-05 08:25:55'),
(7, 2, 'Lionel', 'Messi', 'Delantero', 112000000, 'storage/images/y31QfAJdfaIb4VyXpJIng5gnhCeDC1sJenBpqfUn.png', '2020-05-05 09:01:27', '2020-05-05 09:01:27');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fundation` int(11) NOT NULL,
  `stadium` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coach` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` varchar(2500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `web` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wallpaper` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `teams`
--

INSERT INTO `teams` (`id`, `name`, `image`, `fundation`, `stadium`, `coach`, `nationality`, `review`, `web`, `wallpaper`, `created_at`, `updated_at`) VALUES
(1, 'Real Madrid C.F', 'storage/images/dla5JzslIeT4hoo9OR44TcBw7h0XGtIhLdemSjP1.png', 1902, 'Santiago Bernabéu', 'Zinedine Zidane', 'España', 'holaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholaholaholavvvholaholaholaholaholaholaholaholaholaholaholaholaholaholah', 'https://www.realmadrid.com/', 'storage/images/eaoH2nhVQ1TPkCXSa6A6F5NIlMQRTJkohHzITIM5.jpeg', '2020-05-05 08:06:18', '2020-05-07 07:45:15'),
(2, 'F.C Barcelona', 'storage/images/Qv4tLbJqhg6HsGIaYWQXfPmZIWFmUx2AKf4wb1Fd.png', 1899, 'Camp Nou', 'Quique Setién', 'España', 'holaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaaholaa', 'https://www.fcbarcelona.es/es/', 'storage/images/L3t1buR88OldWpnkWLGrzDbW7EAukSA2Uut1XTxp.jpeg', '2020-05-05 08:06:51', '2020-05-05 14:57:02'),
(3, 'Real Betis', 'storage/images/DydEMo8bI72OPLxCN3E4Dp5H36DgaeNFnV6F4xcy.png', 1900, 'Benito Villamarín', 'Joan Rubí', 'España', 'holaa', 'betis', 'storage/images/cbd9VtHPwlkOH3A3OppPCtkvrx2G2T0IgXi7DHEU.jpeg', '2020-05-05 08:07:21', '2020-05-05 08:07:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fav_team` int(11) NOT NULL,
  `user_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `last_name2`, `email`, `email_verified_at`, `password`, `fav_team`, `user_description`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'David', 'Cuevas', 'Herrero', 'd290397@hotmail.es', NULL, '$2y$10$6x.7l0A81uKBukucr.115eLZod5wZ8XTf3knO1SDfJMbEnR2RMojC', 1, 'hola', 'Administrador', NULL, '2020-05-05 08:03:02', '2020-05-07 07:34:51'),
(2, 'Creador', 'cread', 'crea', 'creador@email.es', NULL, '$2y$10$6x.7l0A81uKBukucr.115eLZod5wZ8XTf3knO1SDfJMbEnR2RMojC', 2, 'hola creador', 'Creador', NULL, '2020-05-07 06:17:26', '2020-05-07 07:35:29'),
(3, 'susi', 'del', 'cornio', 'susi@susi.es', NULL, '$2y$10$zUP/ea1ikXvlueTjC4MDwuKCcmOcVr4j4w61jCA7vpayfpt.mvfNq', 3, 'soy susi', 'Creador', NULL, '2020-05-07 07:27:34', '2020-05-07 07:36:29');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `leagues`
--
ALTER TABLE `leagues`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `matches_id_team_local_foreign` (`id_team_local`),
  ADD KEY `matches_id_team_visitor_foreign` (`id_team_visitor`),
  ADD KEY `matches_id_league_foreign` (`id_league`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notices_id_user_foreign` (`id_user`);

--
-- Indices de la tabla `participates`
--
ALTER TABLE `participates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participates_id_league_foreign` (`id_league`),
  ADD KEY `participates_id_team_foreign` (`id_team`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`id`),
  ADD KEY `players_id_team_foreign` (`id_team`);

--
-- Indices de la tabla `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `leagues`
--
ALTER TABLE `leagues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `matches`
--
ALTER TABLE `matches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT de la tabla `notices`
--
ALTER TABLE `notices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `participates`
--
ALTER TABLE `participates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `players`
--
ALTER TABLE `players`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `matches`
--
ALTER TABLE `matches`
  ADD CONSTRAINT `matches_id_league_foreign` FOREIGN KEY (`id_league`) REFERENCES `leagues` (`id`),
  ADD CONSTRAINT `matches_id_team_local_foreign` FOREIGN KEY (`id_team_local`) REFERENCES `teams` (`id`),
  ADD CONSTRAINT `matches_id_team_visitor_foreign` FOREIGN KEY (`id_team_visitor`) REFERENCES `teams` (`id`);

--
-- Filtros para la tabla `notices`
--
ALTER TABLE `notices`
  ADD CONSTRAINT `notices_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `participates`
--
ALTER TABLE `participates`
  ADD CONSTRAINT `participates_id_league_foreign` FOREIGN KEY (`id_league`) REFERENCES `leagues` (`id`),
  ADD CONSTRAINT `participates_id_team_foreign` FOREIGN KEY (`id_team`) REFERENCES `teams` (`id`);

--
-- Filtros para la tabla `players`
--
ALTER TABLE `players`
  ADD CONSTRAINT `players_id_team_foreign` FOREIGN KEY (`id_team`) REFERENCES `teams` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
