@extends('layouts.master')
@section('content')
    <div class="container mt-5">
        <h1 class="colorFuenteWebInfo1">PRIVACIDAD</h1>
        <p class="colorFuenteWebInfo2">
    En cumplimiento de lo dispuesto en el Reglamento Europeo 2016/679 General de Protección de Datos, te informamos de que trataremos los datos que nos facilitas para:

    Gestionar la contratación de servicios que realice a través de la Plataforma, así como la facturación y entrega correspondiente.
    Remitir periódicamente comunicaciones sobre servicios, eventos y noticias relacionadas con las actividades desarrolladas por , por cualquier medio (teléfono, correo postal o email), salvo que se indique lo contrario o el usuario se oponga o revoque su consentimiento.
    Remitir información comercial y / o promocional relacionada con el sector de servicios contratados y valor añadido para usuarios finales, salvo que se indique lo contrario o el usuario se oponga o revoque su consentimiento.
    Dar cumplimiento a las obligaciones legalmente establecidas, así como verificar el cumplimiento de las obligaciones contractuales, incluía la prevención de fraude.
    Cesión de datos a organismos y autoridades, siempre y cuando sean requeridos de conformidad con las disposiciones legales y reglamentarias.
   </p>

        <h3 class="colorFuenteWebInfo1">Datos identificativos</h3>
        <h5 class="colorFuenteWebInfo1">Metadatos de comunicaciones electrónicas</h5>
        <p class="colorFuenteWebInfo2">Datos de información comercial. En caso de que el usuario facilite datos de terceros, manifiesta contar con el consentimiento de estos y se compromete a trasladarle la información contenida en esta cláusula, eximiendo a de cualquier responsabilidad en este sentido.
    No obstante, podrá llevar a cabo las verificaciones para constatar este hecho, adoptando las medidas de diligencia debida que correspondan, conforme a la normativa de protección de datos.
    El tratamiento de datos cuyo fin es el envío de boletines periódicos (newslettering) sobre servicios, eventos y noticias relacionadas con nuestra actividad profesional se basa en el consentimiento del interesado, solicitado expresamente para llevar a cabo dichos tratamientos, de acuerdo con la normativa vigente.
            Además, la legitimación para el tratamiento de los datos relacionados con ofertas o colaboraciones se basan en el consentimiento del usuario que remite sus datos, que puede retirar en cualquier momento, si bien ello puede afectar a la posible comunicación de forma fluida y obstrucción de procesos que desea realizar.

    Tus datos podrán ser accedidos por aquellos proveedores que prestan servicios a , tales como servicios de alojamiento, herramientas de marketing y sistemas de contenido u otros profesionales, cuando dicha comunicación sea necesaria normativamente, o para la ejecución de los servicios contratados.
        , ha suscrito los correspondientes contratos de encargo de tratamiento con cada uno de los proveedores que prestan servicios a TransferDavid, con el objetivo de garantizar que dichos proveedores tratarán tus datos de conformidad con lo establecido en la legislación vigente.

    También podrán ser cedidos a las Fuerzas y Cuerpos de Seguridad del Estado en los casos que exista una obligación legal.</p>

        <h5 class="colorFuenteWebInfo1">Seguridad de la Información</h5>
        <p class="colorFuenteWebInfo2">Para proteger las diferentes tipologías de datos reflejados en esta política de privacidad llevará a cabo las medidas de seguridad técnicas necesarias para evitar su pérdida, manipulación, difusión o alteración.

    Encriptación de las comunicaciones entre el dispositivo del usuario y los servidores de TransferDavid.
    Encriptación de la información en los propios servidores de TransferDavid.
    Otras medidas que eviten el acceso a los datos del usuario por parte de terceros.
    En aquellos casos en los que cuente con prestadores de servicio para el mantenimiento de la plataforma que se encuentren fuera de la Unión Europea, estas trasferencias internacionales se hayan regularizadas atendiendo al compromiso de con la protección, integridad y seguridad de los datos personales de los usuarios.</p>
        <h5 class="colorFuenteWebInfo1">Derechos</h5>
        <p class="colorFuenteWebInfo2">Tiene derecho a obtener confirmación sobre si en estamos tratando datos personales que te conciernan, o no.
    Asimismo, tienes derecho a acceder a tus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos.

    En determinadas circunstancias, podrás solicitar la limitación del tratamiento de tus datos, en cuyo caso únicamente los conservaremos para el ejercicio o la defensa de reclamaciones.
    En determinadas circunstancias y por motivos relacionados con tu situación particular, podrás oponerte al tratamiento de tus datos. dejará de tratar los datos, salvo por motivos legítimos imperiosos, o el ejercicio o la defensa de posibles reclamaciones.

    Asimismo, puedes ejercer el derecho a la portabilidad de los datos, así como retirar los consentimientos facilitados en cualquier momento, sin que ello afecte a la licitud del tratamiento basado en el consentimiento previo a su retirada.

    Si deseas hacer uso de cualquiera de tus derechos puede dirigirse a TransferDavid.

            Por último, te informamos que puedes dirigirte ante la Agencia Española de Protección de Datos y demás organismos públicos competentes para cualquier reclamación derivada del tratamiento de tus datos personales.</p>

        <h5 class="colorFuenteWebInfo1">Modificación de la política de privacidad</h5>
        <p class="colorFuenteWebInfo2">Se podrá modificar la presente Política de Privacidad en cualquier momento, siendo publicadas las sucesivas versiones en el Sitio Web. En cualquier caso, comunicará con previo aviso las modificaciones de la presente política que afecten a los usuarios a fin de que puedan aceptar las mismas.</p>

        <p class="colorFuenteWebInfo2">La presente Política de Privacidad se encuentra actualizada a fecha 20/05/2020 (España. Reservados todos los derechos.</p>
    </div>
    @endsection