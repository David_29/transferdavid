@extends('layouts.master')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/styles.css')}}">
    <div class="col-md-12 justify-content-center text-center">
        <h1 class="fuenteTitulo mt-5 mb-5 backGroundCabeceras">Jugadores</h1>
                <div class="row mt-3">
                    <div class="col-md-4 col-sm-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <form action="" method="POST" class="estilosFormFiltrarJugador mt-5 pl-5 pr-5" style="">
                                        @csrf
                                        <label for="busquedaJugador" class="fuenteBlanca">Busque su jugador</label>
                                        <input type="text" class="form-control inpJug bg-info" placeholder="Busque su jugador" name="nameFindPlayer">
                                        <label for="selectEquipo" class="fuenteBlanca">Seleccione Equipo</label>
                                        <select class="form-control text-white bg-info" id="selectEquipo" name="teamFindPlayer">
                                            @foreach($teams as $team)
                                                <option value="{{$team->id}}">{{$team->name}}</option>
                                            @endforeach
                                                <option selected="selected">Todos</option>
                                        </select>
                                        <label for="selectPosicion" class="fuenteBlanca">Seleccione Posición</label>
                                        <select class="form-control text-white bg-info" id="positionFindPlayer" name="positionFindPlayer">
                                            <option>Delantero</option>
                                            <option>Centrocampista</option>
                                            <option>Defensa</option>
                                            <option>Portero</option>
                                            <option selected="selected">Todas</option>
                                        </select>
                                        <input type="submit" class="btn btn-info mt-1" value="Buscar">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                    <div class="row">
                        @foreach($jugadores as $jugador)
                        <div class="col-lg-6 col-md-12 mt-5" style="background-color: rgba(0, 233, 255, 0.8);padding-right: 0px !important;padding-left: 0px !important;)">
                            <div class="row">
                                <div class="col-md-1 colorHueco"></div>
                                <div class="col-md-3 " style="border: 1px solid black;">
                                    <img class="img-fluid margenesJugador" style="height: 100px !important;"  src="{{$jugador->player_image}}" alt="Card image cap">
                                </div>
                                <div class="col-md-4" style="border: 1px solid black;">
                                    <h6 class="fuenteBlanca mt-4" >Posición: {{$jugador->position}}</h6>
                                    <h6 class="fuenteOscura mt-4" >{{$jugador->name}} {{$jugador->last_name}}</h6>
                                    <p class="fuenteBlanca mt-4" >Valor de mercado: <span class="fuenteOscura"><b>{{$jugador->value}}€</b></span></p>
                                </div>
                                <div class="col-md-3 " style="border: 1px solid black;">
                                    <img class="img-fluid margenesEquipo" style="height: 100px !important;" src="{{$jugador->image}}" alt="Card image cap">
                                </div>
                                <div class="col-md-1 colorHueco"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-1 colorHueco"></div>
                                <div class="col-md-10">
                                    <form action="" method="POST">
                                        @csrf
                                        @method('PUT')
                                        @if(Auth()->check())
                                            @if(Auth()->user()->role!="Estándar")
                                                <a class="btn btn-success mt-2 mb-2" href="{{url('/editarJugadores/'.$jugador->id)}}">Editar</a>
                                                <a class="btn btn-danger mt-2 mb-2" style="color:white" href="{{url("/jugadoresEliminar/".$jugador->id)}}">Eliminar</a>
                                            @endif
                                        @else
                                            <a class="btn btn-success disabled mt-2 mb-2" href="{{url('/editarJugadores/'.$jugador->id)}}">Editar</a>
                                            <a class="btn btn-danger disabled mt-2 mb-2" style="color:white" href="{{url("/jugadoresEliminar/".$jugador->id)}}">Eliminar</a>
                                        @endif
                                    </form>
                                </div>
                                <div class="col-md-1  colorHueco"></div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    </div>
                </div>
    </div>
@endsection